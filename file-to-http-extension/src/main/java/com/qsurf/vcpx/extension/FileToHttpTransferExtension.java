package com.qsurf.vcpx.extension;

import com.qsurf.vcpx.extension.pipeline.FileToHttpTransferDataSourceFactory;
import com.qsurf.vcpx.extension.util.Logger;
import org.eclipse.edc.api.auth.spi.AuthenticationService;
import org.eclipse.edc.connector.dataplane.http.pipeline.HttpDataSinkFactory;
import org.eclipse.edc.connector.dataplane.http.pipeline.HttpSinkRequestParamsSupplier;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataTransferExecutorServiceContainer;
import org.eclipse.edc.connector.dataplane.spi.pipeline.PipelineService;
import org.eclipse.edc.runtime.metamodel.annotation.Extension;
import org.eclipse.edc.runtime.metamodel.annotation.Inject;
import org.eclipse.edc.runtime.metamodel.annotation.Setting;
import org.eclipse.edc.spi.http.EdcHttpClient;
import org.eclipse.edc.spi.security.Vault;
import org.eclipse.edc.spi.system.ServiceExtension;
import org.eclipse.edc.spi.system.ServiceExtensionContext;
import org.eclipse.edc.web.spi.WebService;

@Extension(value = FileToHttpTransferExtension.NAME)
public class FileToHttpTransferExtension implements ServiceExtension {
    private final Logger logger;

    public static final String NAME = "Data Plane File to HTTP Transfer";
    private static final int DEFAULT_PART_SIZE = 5;

    @Setting
    private static final String EDC_DATAPLANE_HTTP_SINK_PARTITION_SIZE = "edc.dataplane.http.sink.partition.size";

    //Updated milestone-7 to milestone-8
//    @Inject
//    private OkHttpClient httpClient;
    @Inject
    private EdcHttpClient httpClient;
    @Inject
    private PipelineService pipelineService;
    @Inject
    private DataTransferExecutorServiceContainer executorContainer;
    @Inject
    private Vault vault;

    public FileToHttpTransferExtension() {
        this.logger = Logger.getInstance();
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void initialize(ServiceExtensionContext context) {
        var monitor = context.getMonitor();
        var sinkPartitionSize = context.getSetting(EDC_DATAPLANE_HTTP_SINK_PARTITION_SIZE, DEFAULT_PART_SIZE);

        logger.setMonitor(monitor);

        var sourceFactory = new FileToHttpTransferDataSourceFactory();
        pipelineService.registerFactory(sourceFactory);

        var sinkParamsSupplier = new HttpSinkRequestParamsSupplier(vault, context.getTypeManager());
        var sinkFactory = new HttpDataSinkFactory(httpClient, executorContainer.getExecutorService(), sinkPartitionSize, monitor, sinkParamsSupplier);
        pipelineService.registerFactory(sinkFactory);

        logger.log("File to Http Transfer Extension initialized!");
    }
}
